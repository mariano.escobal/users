import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { AwsClient } from './client/aws/aws.client';
async function bootstrap() {
    const app = await NestFactory.create(AppModule);

    const config = new DocumentBuilder()
        .setTitle('cl-ccom-nestjs-template')
        .setDescription('microservice description')
        .setVersion('1.0')
        .addTag('Products')
        .build();

    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('api', app, document);

    await app.listen(3001);
    const appService = app.get(AwsClient);
    await appService.getSecret();
}
bootstrap();
