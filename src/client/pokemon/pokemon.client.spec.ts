import { HttpModule } from '@nestjs/axios';
import { HttpException, HttpStatus } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { of, throwError } from 'rxjs';
import { PokemonClient } from './pokemon.client';

let pokemonClient: PokemonClient;

beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
        providers: [PokemonClient],
        imports: [HttpModule],
    }).compile();
    pokemonClient = app.get<PokemonClient>(PokemonClient);
});

it('should be defined', () => {
    expect(pokemonClient).toBeDefined();
});

describe('getPokemon', () => {
    it('should call /getPokemon success', async () => {
        const pokemonBodyResponse = { name: 'pikachu', height: 50 };
        jest.spyOn(pokemonClient['httpService'], 'get')
            .mockReturnValue(of({ data: pokemonBodyResponse, status: 200, statusText: 'OK', headers: {}, config: {}}));
        let data = {};

        pokemonClient.getPokemon('pikachu').subscribe({
            next: (val) => {
                data = val;
            },
            complete: () => {
                expect(data).toEqual(pokemonBodyResponse);
            },
        });
    });
    it('should call /getPokemon with failure', async () => {
        const error = new HttpException({
            status: 403,
            error: 'Not Found',
        }, HttpStatus.NOT_FOUND);
        jest.spyOn(pokemonClient['httpService'], 'get').mockReturnValue(throwError(() => error));

        pokemonClient.getPokemon('pikachu').subscribe({
            error: (err) => {
                expect(err.response.status).toBe(403);
            },
        });
    });
});
