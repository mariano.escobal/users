import { HttpException, Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
// eslint-disable-next-line import/named
import { AxiosResponse } from 'axios';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class PokemonClient {
    constructor(private readonly httpService: HttpService) {

    }

    getPokemon(name: string): Observable<AxiosResponse> {
        const headers = {
            'Content-Type': 'application/json',
        };
        return (this.httpService.get
        (`https://pokeapi.co/api/v2/pokemon/${name}`,
            { headers }).pipe(
            map(response => response.data),
            catchError(error => {
                throw new HttpException({
                    status: error.response.status,
                    error: error.response.statusText,
                }, error.response.status);
            })
        ));
    }
}
