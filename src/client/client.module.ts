import { Global, Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { AwsClient } from './aws/aws.client';
import { PokemonClient } from './pokemon/pokemon.client';

@Global()
@Module({
    imports: [
        HttpModule.register({
            timeout: 15000,
            maxRedirects: 5,
        }),
    ],
    providers: [AwsClient, PokemonClient],
    exports: [AwsClient, PokemonClient],
})
export class ClientModule {}
