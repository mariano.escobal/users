import { HttpModule } from '@nestjs/axios';
import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from '@nestjs/config';
import { of, throwError } from 'rxjs';
import { HttpException, HttpStatus } from '@nestjs/common';
import { AwsClient } from './aws.client';

let awsClient: AwsClient;

const mockSecretValue = jest.fn();

beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
        providers: [AwsClient, ConfigService],
        imports: [HttpModule],
    }).compile();
    awsClient = module.get<AwsClient>(AwsClient);
    mockSecretValue.mockReset();
});

afterEach(() => {
    jest.clearAllMocks();
});

it('gets secrets from aws', async () => {
    const awsBodyResponse = { base: { SCHEMA_SECURE: 'https' }, addresses: { SCHEMA_INSECURE: 'http' }};
    jest.spyOn(awsClient['httpService'], 'get')
        .mockReturnValue(of({ data: awsBodyResponse, status: 200, statusText: 'OK', headers: {}, config: {}}));
    await awsClient.getSecret();
    expect(process.env.SCHEMA_SECURE).toEqual('https');
});

it('throws error getting secrets from aws', async () => {
    jest.spyOn(global.console, 'error');
    const error = new HttpException({
        status: 403,
        error: 'Not Found',
    }, HttpStatus.NOT_FOUND);
    const expectedError = 'An error has occured retrieving secrets. Redeploy';
    jest.spyOn(awsClient['httpService'], 'get').mockReturnValue(throwError(() => error));
    await awsClient.getSecret();
    expect(console.error).toHaveBeenCalledWith(Error(expectedError));
});
