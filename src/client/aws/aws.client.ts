import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { map, catchError, throwError } from 'rxjs';
import { AWS_SECRETS } from '../../constants/constants';
import { ERRORS } from '../../constants/errors';

@Injectable()
export class AwsClient {
    constructor(private readonly httpService: HttpService) {
    }
    getSecret() {
        const config = {
            headers: {
                apikey: process.env.API_KEY_AWS,
                'Content-Type': 'application/json',
            },
            params: { secretName: `supermarkets-${process.env.STORE}` },
        };
        this.httpService.get(AWS_SECRETS.url, config)
            .pipe(
                map(secrets => {
                    const baseSecrets = secrets.data[AWS_SECRETS.base];
                    const moduleSecrets = secrets.data[AWS_SECRETS.module];
                    Object.keys(baseSecrets).forEach((att) => {
                        process.env[att] = baseSecrets[att];
                    });
                    Object.keys(moduleSecrets).forEach((att) => {
                        process.env[att] = moduleSecrets[att];
                    });
                }),
                catchError(() => throwError(() => new Error(ERRORS.SECRET_ERROR))))
            .subscribe({
                error: (err) => {
                    console.error(err);
                },
            });
    }
}