export const AWS_REGION = 'us-east-1';
export const AWS_SECRET_STRING = 'SecretString';
export const AWS_SECRET_NAME = 'cl-{app}-template';
export const AWS_SECRETS = {
    url: 'https://internal-api-supermarketsapp.ecomm.cencosud.com/supermarkets/awssecrets/getSecrets',
    base: 'base',
    module: 'addresses',
};