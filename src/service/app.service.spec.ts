import { Test, TestingModule } from '@nestjs/testing';
import { PokemonClient } from '../client/pokemon/pokemon.client';
import { AppService } from './app.service';

let appService: AppService;

const mockAWSClientPokemon = {
    getPokemon: jest.fn(),
};

beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
        providers: [AppService, PokemonClient],
    })
        .overrideProvider(PokemonClient)
        .useValue(mockAWSClientPokemon)
        .compile();

    appService = app.get<AppService>(AppService);
});

describe('tests relative to getHello endpoint', () => {
    it('returns Hello! message', () => {
        expect(appService.getHello()).toBe('Hello!');
    });
});

describe('tests relative to setHello endpoint', () => {
    it('returns requested input', () => {
        expect(appService.setHello('Hello')).toBe('El POST devuelve Hello');
    });
});

describe('tests relative to getPokemon endpoint', () => {
    it('returns requested pokemon details', () => {
        mockAWSClientPokemon.getPokemon.mockImplementation(() => ({
            name: 'Pikachu',
        }));
        expect(appService.getPokemon('Pikachu')).toEqual({ name: 'Pikachu' });
    });
});
