import { Injectable } from '@nestjs/common';
import { User } from '../dto/user.dto';
import { CreateUserDto } from '../dto/create.user.dto';
import { UpdateUserDto } from '../dto/update.user.dto';

@Injectable()
export class AppService {
    constructor() {}
    create(createUserDto: CreateUserDto) {
        //verificar elementos
            //elementos validos
            //no exista otro email
        //generar id por si no se usa base uuid
        //Crear
        //retornar ok
        return 'This action adds a new user';
    }
    findOne(id:string):User{
        //buscar usuario si no existe enviar un null

        return 
    }
    findAll() {
        return `This action returns all users`;
    }
    
    update(id: number, updateUserDto: UpdateUserDto) {
        return `This action updates a #${id} user`;
    }
    
    remove(id: number) {
        return `This action removes a #${id} user`;
    }

    changePassword(){}

    login(){}

    logout(){}
}
