import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { HttpService } from '@nestjs/axios';
import { of } from 'rxjs';
import { AppModule } from './app.module';

let app: INestApplication;

let httpService: HttpService;

beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
        imports: [AppModule],
        providers: [],
    }).compile();

    app = moduleFixture.createNestApplication();

    httpService = app.get<HttpService>(HttpService);

    await app.init();
});

afterAll(async () => {
    await app.close();
});

it('/health (GET)', () => {
    return request(app.getHttpServer())
        .get('/health')
        .expect(200);
});

it('/getHello (GET)', () => {
    return request(app.getHttpServer())
        .get('/getHello')
        .expect(200).expect('Hello!');
});

it('/setHello (POST)', () => {
    return request(app.getHttpServer())
        .post('/setHello')
        .set('Accept', 'application/json')
        .send({ value: 'Hello!' })
        .expect(201).expect('El POST devuelve Hello!');
});

it('/getPokemon', () => {
    jest.spyOn(httpService, 'get')
        .mockReturnValue(of({ data: { name: 'Pikachu', height: 30 }, status: 200, statusText: 'OK', headers: {}, config: {}}));
    return request(app.getHttpServer())
        .get('/getPokemon')
        .set('Accept', 'application/json')
        .query({ name: 'Pikachu' })
        .expect(200).expect({ name: 'Pikachu', height: 30 });
});
