import { Module } from '@nestjs/common';
import { AppController } from './controller/app.controller';
import { AppService } from './service/app.service';
import { ClientModule } from './client/client.module';

@Module({
    imports: [
        ClientModule,
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {}
