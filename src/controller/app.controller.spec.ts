import { Test, TestingModule } from '@nestjs/testing';
import { AppService } from '../service/app.service';
import { AppController } from './app.controller';

describe('AppController', () => {
    let appController: AppController;

    const mockAppService = {
        getHello: jest.fn(() => {
            return 'Hello!';
        }),
        setHello: jest.fn(() => {
            return 'El POST devuelve Hello!';
        }),
        getPokemon: jest.fn(() => {
            return { name: 'Pikachu', height: 30 };
        }),
    };
    beforeEach(async () => {
        const app: TestingModule = await Test.createTestingModule({
            controllers: [AppController],
            providers: [AppService],
        })
            .overrideProvider(AppService)
            .useValue(mockAppService)
            .compile();

        appController = app.get<AppController>(AppController);
    });

    describe('should call getHello', () => {
        it('/ (GET)', () => {
            expect(appController.getHello()).toBe('Hello!');
        });
    });

    describe('should call health', () => {
        it('/health (GET)', () => {
            expect(appController.getHealth()).toEqual({
                ok: true,
            });
        });
    });

    describe('should call setHello', () => {
        it('/setHello (POST)', () => {
            expect(appController.setHello({ value: 'Hello!' })).toEqual(
                'El POST devuelve Hello!',
            );
        });
    });

    describe('should call getPokemon', () => {
        it('/getPokemon (GET)', () => {
            expect(appController.getPokemon({ name: 'pikachu' })).toEqual({
                name: 'Pikachu',
                height: 30,
            });
        });
    });
});
