FROM registry.gitlab.com/cencosud-ds/cencommerce/utils/docker-images/node:16.15-alpine3.16 AS builder
WORKDIR /app
COPY package.json ./
COPY package-lock.json ./
COPY .npmrc ./
ARG ENVIRONMENT
ARG STORE
ARG API_KEY_AWS
ARG APP_PORT
ENV ENVIRONMENT ${ENVIRONMENT}
ENV STORE ${STORE}
ENV API_KEY_AWS ${API_KEY_AWS}
ENV APP_PORT ${APP_PORT}
RUN npm install glob rimraf
RUN npm install
COPY . .
RUN npm run build

FROM registry.gitlab.com/cencosud-ds/cencommerce/utils/docker-images/node:16.15-alpine3.16
WORKDIR /app
ARG NEW_RELIC_APP_NAME
ARG NEW_RELIC_LABELS
ENV NEW_RELIC_APP_NAME=$NEW_RELIC_APP_NAME
ENV NEW_RELIC_LABELS=$NEW_RELIC_LABELS
ARG NEW_RELIC_LICENSE_KEY
ENV NEW_RELIC_LICENSE_KEY=$NEW_RELIC_LICENSE_KEY
ARG NEW_RELIC_NO_CONFIG_FILE
ENV NEW_RELIC_NO_CONFIG_FILE=${NEW_RELIC_NO_CONFIG_FILE}
COPY --from=builder /app ./
EXPOSE ${APP_PORT}
CMD npm run start:${ENVIRONMENT}.${STORE}